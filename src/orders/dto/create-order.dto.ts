class CreateOrderItemDto {
  productId: number;
  amount: null;
}

export class CreateOrderDto {
  customerId: number;
  orderItems: CreateOrderItemDto[];
}
