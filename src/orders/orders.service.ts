import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Customer } from 'src/customers/entities/customer.entity';
import { Product } from 'src/products/entities/product.entity';
import { Repository } from 'typeorm';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { OrderItem } from './entities/order-item';
import { Order } from './entities/order.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private ordersRepository: Repository<Order>,
    @InjectRepository(Customer)
    private customersRepository: Repository<Customer>,
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
  ) {}

  async create(createOrderDto: CreateOrderDto) {
    console.log(createOrderDto);
    const customer = await this.customersRepository.findOneBy({
      id: createOrderDto.customerId,
    });
    const orderItem: OrderItem[] = createOrderDto.orderItems.map((od) => {
      const orderItem = new OrderItem();
      orderItem.amount = od.amount;
      return orderItem;
    });
    console.log(customer);
    console.log(orderItem);
    return 'This action adds a new order';
  }

  findAll() {
    return this.ordersRepository.find({
      relations: ['customer', 'orderItems'],
    });
  }

  findOne(id: number) {
    return `This action returns a #${id} order`;
  }

  update(id: number, updateOrderDto: UpdateOrderDto) {
    return `This action updates a #${id} order`;
  }

  remove(id: number) {
    return `This action removes a #${id} order`;
  }
}
