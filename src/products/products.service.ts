import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

@Injectable()
export class ProductsService {
  ordersREpository: any;
  constructor(
    @InjectRepository(Product)
    private productRepository: Repository<Product>,
  ) {}

  create(createProductDto: CreateProductDto) {
    return 'This action adds a new product';
  }

  findAll() {
    return this.ordersREpository.find({});
  }

  async findOne(id: number) {
    const customer = await this.ordersREpository.findOne({
      where: { id: id },
      relations: ['orders'],
    });
    if (!customer) {
      throw new NotFoundException();
    }
    return customer;
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const customer = await this.ordersREpository.findOneBy({ id: id });
    if (!customer) {
      throw new NotFoundException();
    }
    const updateCustomer = { ...customer, ...updateProductDto };
    return this.ordersREpository.save(updateProductDto);
  }

  async remove(id: number) {
    const customer = await this.ordersREpository.findOneBy({ id: id });
    if (!customer) {
      throw new NotFoundException();
    }
    return this.ordersREpository.softRemove(customer);
  }
}
